import React, { useState } from 'react'

import dummyData from '../data.json'
function SearchBar () {
  const [searchItem, setSearchItem] = useState('')
  const [inputText, setInputText] = useState('')

  const data = dummyData.filter((val) => {
    if (searchItem === '') {
      return val
    } else {
      return val.first_name.toLowerCase().includes(searchItem.toLowerCase())
    }
  })

  // this function work on onChange Event
  const showData = (e) => {
    setTimeout(() => setSearchItem(e.target.value), 3000)
  }

  // this function work on onClick Event

  const showEnterData = (e) => {
    setSearchItem('')
    setInputText(e.target.value)
  }
  const delayData = (e) => {
    setSearchItem(inputText)
    console.log('inputText', inputText)
  }

  const tableData = data.map((value, key) => {
    return (
      <tr key={key}>
        <td>{value.id}</td>
        <td>{value.first_name}</td>
        <td>{value.last_name}</td>
        <td>{value.email}</td>
        <td>{value.gender}</td>
      </tr>
    )
  })
  return (
    <>
    <div className="title">
        <h1>Search bar Task</h1>
      </div>
      <div className="search">
        <input
          type="text"
          placeholder="search..... onChange"
          onChange={(e) => {
            showData(e)
          }}
        />
        <input
          type="text"
          placeholder="search.....Enter"
          onChange={(e) => {
            showEnterData(e)
          }}
        />
        <button className='btn btn-warning' onClick={() => delayData()}>Search</button>
      </div>
      <div className="container">
        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th>Id</th>
              <th>FirstName</th>
              <th>LastName</th>
              <th>Email</th>
              <th>Gender</th>
            </tr>
          </thead>
          <tbody>{tableData}</tbody>
        </table>
      </div>
    </>
  )
}

export default SearchBar
